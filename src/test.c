#include "test.h"

struct block_header* mem_block1;
struct block_header* mem_block2;

 bool test1(void) {
    printf("\nTest 1: Memory allocation\n");
    size_t test_size = 500;
    void *malloc_1 = _malloc(test_size);
    debug_heap(stdout, mem_block1);
    size_t capacity = mem_block1->capacity.bytes;
    bool res = (capacity == test_size && malloc_1 != NULL);
    if (res) {
    	printf("Test 1: Succes\n");
    	return true;
    } else {
    	printf("Test 1: Fail\n");
    	return false;
    }
     _free(malloc_1);
}


 bool test2(void) {
	printf("\nTest 2: Freeing one block from several allocated ones\n");
    void *mem1 = _malloc(3100);
    void *mem2 = _malloc(500);
    debug_heap(stdout, mem_block1);

    size_t block_number_before = get_number_of_blocks(mem_block1);
    size_t block_number_after = get_number_of_blocks(mem_block1);
    _free(mem2);
    _free(mem1);
    if((block_number_before - block_number_after) == 1){
        printf("Test 2: Succes\n");
    	return true;
    }else{
        printf("Test 2: Fail\n");
    	return false;
    } 

  
}


 bool test3(void) {
	printf("\nTest 3: Freeing two blocks from several allocated ones\n");
    void *mem1 = _malloc(3000);
    void *mem2 = _malloc(2000);
    void *mem3 = _malloc(1000);
    debug_heap(stdout, mem_block1);
    _free(mem3);
    _free(mem2);
    size_t block_number_after = get_number_of_blocks(mem_block1);
    _free(mem1);
    if (block_number_after == 1) {
        printf("Test 3: Succes\n");
    	return true;
    }else{
        printf("Test 3: Fail\n");
    	return false;
    }
}

 bool test4(void) {
	printf("\nTest 4: Memory is over, the new memory region expands the old one\n");
    void *mem1 = _malloc(5000);
    void *mem2 = _malloc(4000);
    void *mem3 = _malloc(3000);
    bool flag;
    debug_heap(stdout, mem_block1);
    size_t block_number = get_number_of_blocks(mem_block1);
    if (block_number == 3) {
        printf("Test 4: Succes\n");
    	flag = true;
    } else {
    	printf("Test 4: Fail\n");
    	flag= false;
    } 
    _free(mem1);
    _free(mem2);
    _free(mem3);
    return flag;
}

bool test5(void) {
    printf("\nTest 5: Memory has run out, the old memory region cannot be expanded due to another allocated address range\n");
    void *malloc_1 = _malloc(13000);
     void *malloc_2 = _malloc(7000);

    struct block_header *block_1 = get_header_of_block(malloc_1);
    debug_heap(stdout, block_1);
    struct block_header * next_block = block_1->next;
    void*  tmp = next_block->contents + next_block->capacity.bytes;
    void*  mtmp = mmap(tmp, 256, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    if (mtmp == NULL) {
    	return false;
    }
   
    debug_heap(stdout, block_1);
    struct block_header *block_2 = get_header_of_block(malloc_2);
    bool check_1 = !block_1->is_free;
    bool check_2 = !block_2->is_free;
    if (check_1 && check_2) {
        _free(malloc_1);
        _free(malloc_2);
        debug_heap(stdout, block_1);
    }
    printf("Test 5: Succes\n");
    return true;
}
