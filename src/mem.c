#include <stdarg.h>
#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static inline void* split_addr_block(struct block_header* block, size_t query) {
    return (void*) ((uint8_t*) block->contents + query);
}

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static inline size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static inline size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static inline size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);

void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    size_t new_region = region_actual_size(size_from_capacity((block_capacity) {.bytes = query}).bytes);
    void *reg_address = map_pages(addr, new_region, MAP_FIXED_NOREPLACE);
    if (reg_address == MAP_FAILED) {
        reg_address = map_pages(addr, new_region, 0);
        if (reg_address == MAP_FAILED) {
            return REGION_INVALID;
        }
        block_init(reg_address, (block_size) {.bytes = new_region}, NULL);
        return (struct region) {reg_address, new_region, false};
    }
    block_init(reg_address, (block_size) {.bytes = new_region}, NULL);
    return (struct region) {reg_address, new_region, true};
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    return region.addr;
}


/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (!block_splittable(block, query)) {
      return false;
    }      
    block_size block_next_size = {
            .bytes = block->capacity.bytes - query
    };
    void* splitted_addr = split_addr_block(block, query);
    block_init(splitted_addr, block_next_size, block->next);
    block->next = splitted_addr;
    block_capacity block_previous_capacity = {.bytes = query};
    block->capacity = block_previous_capacity;
    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if(block == NULL){
        return false;
    }
    struct block_header *next = block->next;
    bool helper = next == NULL || !mergeable(block, next);
    const bool block_cant_be_continued = helper;
    if (block_cant_be_continued) {
        return false;
    }
    const block_size block_size = size_from_capacity(next->capacity);
    const size_t new_size = block_size.bytes;
    block->capacity.bytes += new_size;
    block->next = next->next;
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    if (!block) {
        return (struct block_search_result) {
                .type = BSR_CORRUPTED,
                .block = NULL
        };
    }
    struct block_header* current_block = block;
    struct block_header* last_block;
    while (current_block) {
        while (try_merge_with_next(current_block));
        if(current_block->is_free && block_is_big_enough(sz, current_block)) {
            return (struct block_search_result) {
                    .type = BSR_FOUND_GOOD_BLOCK,
                    .block = current_block
            };
        }
        last_block = current_block;
        current_block = current_block->next;
    }
    return (struct block_search_result) {
            .type = BSR_REACHED_END_NOT_FOUND,
            .block = last_block
    };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result result = find_good_or_last(block, query);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {     
        split_if_too_big(result.block, query);
        result.block->is_free = false;
    } 
    return result;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    struct region reg = alloc_region(block_after(last), size_max(query, BLOCK_MIN_CAPACITY));
    if(region_is_invalid(&reg)){
        return NULL;
    }
    reg.extends = true;
    last->next = reg.addr;
    if (try_merge_with_next(last)) {
        return last;
    }
    return reg.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result blck = try_memalloc_existing(query, heap_start);
    if (blck.type == BSR_CORRUPTED) {
      return NULL;
    }
    if (blck.type == BSR_FOUND_GOOD_BLOCK) { 
        return blck.block;
    }

    struct block_header* hs = grow_heap(blck.block, query);
    if (hs == NULL) {
      return NULL;
    }
    blck = try_memalloc_existing(query, heap_start);
    if (blck.type == BSR_FOUND_GOOD_BLOCK) {
      return blck.block;
    }
    return NULL;

}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
    if (addr){
        return addr->contents;
    } else {
        return NULL;
    }
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}


void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
