#ifndef MEMORY_ALLOCATOR_HELP_H
#define MEMORY_ALLOCATOR_HELP_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

size_t get_number_of_blocks(struct block_header *block);
struct block_header* get_header_of_block(void* contents);

#endif //MEMORY_ALLOCATOR_HELP_H

