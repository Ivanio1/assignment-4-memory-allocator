#include "help.h"


//Вспомогательные функции
size_t get_number_of_blocks(struct block_header *block) {
    size_t count = 0;

    while (block->next != NULL){
        count++;
        block = block->next;
    }

    return count;
}
struct block_header* get_header_of_block(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
