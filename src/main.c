#include "test.h"

int main(void) {
    bool res = true;
    for (size_t i = 1; i < 6; i++) {
        bool iterable_res = false;
        if (i==1) {
        	iterable_res = test1();
        }
        if (i==2) {
        	iterable_res = test2();
        }
        if (i==3) {
        	iterable_res = test3();
        }
        if (i==4) {
        	iterable_res = test4();
        }
        if (i==5) {
        	iterable_res = test5();
        }
        if (iterable_res) {
            printf("\nTest %zu passed\n", i);
        } else {
            printf("\nTest %zu failed\n", i);
            res = false;
        }
    }
    
    if (res == true) {
    	printf("All tests passed! \n");
    	return 0;   
    } else {
    	printf("All tests failed\n");
    	return 1;
    }
}
